from airflow import DAG
from airflow.operators.python import PythonOperator
from datetime import datetime
import pandas as pd
import statistics as sts


dag = DAG('dag_pythonOperator', description='Exemplo de Dag branch',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


def data_cleaner():
    dataset = pd.read_csv("/opt/airflow/data/Churn.csv", sep=";")
    dataset.columns = ["Id", "Score", "Estado", "Gênero", "Idade", "Patrimônio", "Saldo", "Produtos", "TemCartCrédito", "Ativo", "Salário", "Saiu"]
    
    mediana = sts.median(dataset['Salário'])
    dataset['Salário'].fillna(mediana, inplace=True)
    
    dataset['Gênero'].fillna('Masculino', inplace=True)
    
    mediana = sts.median(dataset['Idade'])
    dataset.loc[(dataset['Idade']<0) | (dataset["Idade"]> 120), 'Idade'] = mediana
    
    dataset.drop_duplicates(subset='Id', keep='first', inplace=True)
    
    dataset.to_csv("/opt/airflow/data/Churn_Clean.csv", sep=";", index=False)
    
    
t1 = PythonOperator(task_id='t1', python_callable=data_cleaner, dag=dag)

t1