from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.trigger_dagrun import TriggerDagRunOperator

from datetime import datetime

dag = DAG('dag_run_dag_1', description='Teste Dag Run Other Dag',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


task1 = BashOperator(task_id='tsk1', bash_command="sleep 5", dag=dag)
task2 = TriggerDagRunOperator(task_id='tsk2', trigger_dag_id='dag_run_dag_2', dag=dag)



task1 >> task2