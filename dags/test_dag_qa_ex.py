"""A liveness prober dag for monitoring composer.googleapis.com/environment/healthy."""
import airflow
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.dummy_operator import DummyOperator
from datetime import timedelta
from datetime import datetime
from google.cloud import storage

import sys
sys.path.append("..")


PROJECT_ID = "teste_qa"

default_dag_args = {
    "owner": "airflow",
    "start_date": airflow.utils.dates.days_ago(1),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "catchup": False,
    "project_id": PROJECT_ID
}



# Bucket GCP
def read_file(file_name):
    read_storage_client = storage.Client()
    bucket_name = "swat_bucket"
    prefix_gcp = "qa_swatteam/radarscpc/202304251530/gcp"

    bucket_gcp = read_storage_client.get_bucket(bucket_name)
    blobs_gcp = bucket_gcp.list_blobs(prefix=prefix_gcp)
    
    for blob in blobs_gcp:
        if blob.name.endswith(file_name):
            data = blob.download_as_string()
            return data.decode('utf-8')

def timestampToInt(timestamp):
    return int(datetime.fromtimestamp(int(float(timestamp))).timestamp())

def getTimestamp(dag_run):
    return timestampToInt(dag_run.conf.get("timestamp", dag_run.start_date.timestamp() - 3600 * 3))


nome_arquivo = "arquivo_teste.txt"




default_args = {
    'start_date': airflow.utils.dates.days_ago(0),
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}


#############################################################
#####                                                   #####
#####                      DAG                          #####
#####                                                   #####
#############################################################


with DAG(
    "test_dag_qa_ex",
    description='liveness monitoring dag',
    schedule_interval='*/20 * * * *',
    default_args=default_dag_args,
    catchup=False,
    max_active_runs=2,
        user_defined_filters={
            'getTimestamp': getTimestamp
        }
) as dag:


    start = DummyOperator(task_id="start")

    # priority_weight has type int in Airflow DB, uses the maximum.



    #### incluindo a partir daqui

    teste_arquivo = PythonOperator(
        task_id='teste_arquivo',
        python_callable = read_file,
        op_kwargs = {'file_name': nome_arquivo}
    )


start >> teste_arquivo

# capturar arquivo dentro de um bucket no GCP
# ler arquivo dentro de um determinado bucket
# como configurar a DAG
# definir fluxo da DAG se sucesso gera um log de sucesso, se foi falha gera um log de falha