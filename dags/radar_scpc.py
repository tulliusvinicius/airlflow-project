# Como disparar a DAG:
# Na aba Trigger DAG use o json abaixo:
#
# {
#     "idExecution": "YYYYMMDD"
#     "job": "radarScpc"
# }
#
# onde YYYYMMDD deve ser substituÃ­do pela data do dia corrente (Ex: 20230413)

import airflow
from airflow import DAG, models

from airflow.operators.python_operator import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator
from airflow.hooks.base_hook import BaseHook
from datetime import datetime
from urllib.parse import unquote

import sys
sys.path.append("..")
import importlib

dataprocUtils = importlib.import_module("migracao-as400-piloto-radarscpc-pipeline.radar_scpc_module.dataprocUtils")
bucket_utils = importlib.import_module("migracao-as400-piloto-radarscpc-pipeline.radar_scpc_module.bucket_utils")
utils = importlib.import_module("migracao-as400-piloto-radarscpc-pipeline.radar_scpc_module.utils")

#==========================SET keys(Retirar quando configurar no ambiente Airflow)
# Defining Constants
PROJECT_ID = models.Variable.get("RADAR_SCPC_GCP_PROJECT")
ENV = models.Variable.get("RADAR_SCPC_ENV")
PARTITIONS = models.Variable.get('radar_partitions', default_var = 2)
CLUSTER_NAME = "migracao-as400-radar-scpc-" + f"{int(datetime.now().timestamp())}"
ID_PROJECT_BQ = models.Variable.get("RADAR_SCPC_GCP_PROJECT_ID_BQ")

default_dag_args = {
    "owner": "airflow",
    "start_date": airflow.utils.dates.days_ago(1),
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "catchup": False,
    "project_id": PROJECT_ID
}

def getProcessingDate(idExecution):
    date = idExecution[:8] # get the first 8 characters of the string
    formatted_date = f"{date[:4]}-{date[4:6]}-{date[6:8]}"
    print(f'Data de processamento: {formatted_date}')
    return formatted_date

def getParameters(**kwargs):
    task_instance = kwargs['task_instance']
    dag_run = kwargs['dag_run']

    pathPrefix = 'radarScpc/'
    pathSufix = 'processing/'
    idExecution = dag_run.conf["idExecution"]
    job = dag_run.conf["job"]

    task_instance.xcom_push(key = 'pipeline', value = job.capitalize())
    task_instance.xcom_push(key = 'pathPrefix', value = pathPrefix)
    task_instance.xcom_push(key = 'pathSufix', value = pathSufix)
    task_instance.xcom_push(key = 'outputFolder', value = idExecution)
    task_instance.xcom_push(key = 'clusterName', value = CLUSTER_NAME)
    task_instance.xcom_push(key = 'processingDate', value = getProcessingDate(idExecution))
    task_instance.xcom_push(key = 'project', value = ID_PROJECT_BQ)

def timestampToInt(timestamp):
    return int(datetime.fromtimestamp(int(float(timestamp))).timestamp())

def getTimestamp(dag_run):
    return timestampToInt(dag_run.conf.get("timestamp", dag_run.start_date.timestamp() - 3600 * 3))

#############################################################
#####                                                   #####
#####                      DAG                          #####
#####                                                   #####
#############################################################

with DAG(
    "radar_scpc",
    schedule_interval=None,
    default_args=default_dag_args,
    tags=["radar", "scpc", "as400"],
    max_active_runs=15,
        user_defined_filters={
            'getTimestamp': getTimestamp
        }
) as dag:

    start = DummyOperator(task_id="start")

    get_parameters = PythonOperator(
        task_id = 'get_parameters',
        python_callable = getParameters,
        provide_context = True
    )

    # Criar cluster dataproc para executar job Spark
    #create_cluster_dataproc = DummyOperator(task_id="create_cluster_dataproc", trigger_rule="none_failed_or_skipped")
    create_cluster_dataproc = PythonOperator(
        task_id="create_cluster_dataproc",
        python_callable=dataprocUtils.create_cluster,
        provide_context=True,
        op_kwargs={"cluster_name": '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "clusterName") }}'},
        trigger_rule="none_failed_or_skipped",
    )


    # Submeter job Spark no cluster dataproc
    # submit_job = DummyOperator(task_id="submit_job_radar_scpc", trigger_rule="all_success")
    submit_job = PythonOperator(
        task_id="submit_job_radar_scpc",
        python_callable=dataprocUtils.submit_spark_job,
        provide_context=True,
        op_kwargs={
            "task_id": "submit_job_radar_scpc",
            "job_name": "SparkApplication-migracao_as400_radar_scpc-" + f"{int(datetime.now().timestamp())}",
            "cluster_name": '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "clusterName") }}',
            "dataproc_spark_properties": {
                'spark.driver.extraJavaOptions' : f'-Dcore.env={ENV} -Dcore.partitions={ PARTITIONS } -Dcore.runId=load_radar_scpc_export_{{{{ ts_nodash }}}} -Dcore.pipeline.conf=classpath:///pipeline' + '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "pipeline") }}' + '.json',
                'spark.executor.heartbeatInterval' : '360s',
                'spark.network.timeout': '10000s',
                'spark.sql.crossJoin.enabled': 'true',
                'spark.properties.timestamp': '{{ dag_run | getTimestamp }}',
                'spark.propertiesCustom.pathPrefix': '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "pathPrefix") }}',
                'spark.propertiesCustom.pathSufix': '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "pathSufix") }}',
                'spark.propertiesCustom.processingDate': '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "processingDate") }}',
                'spark.propertiesCustom.outputFolder': '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "outputFolder") }}' + '/',
                'spark.propertiesCustom.projectBqId': '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "project") }}'
            },
        },
        trigger_rule="all_success"
    )

    # Excluir cluster dataproc da execuÃ§Ã£o do job Spark
    #delete_cluster_dataproc = DummyOperator(task_id="delete_cluster_dataproc", trigger_rule="none_failed_or_skipped")
    delete_cluster_dataproc = PythonOperator(
        task_id="delete_cluster_dataproc",
        python_callable=dataprocUtils.delete_cluster,
        provide_context=True,
        op_kwargs={"cluster_name": '{{ task_instance.xcom_pull(task_ids = "get_parameters", key = "clusterName") }}'},
        trigger_rule="none_failed_or_skipped",
    )

    # Criar arquivo stick de falha
    stick_file_failed = PythonOperator(
        task_id="stick_file_failed",
        python_callable=utils.creat_stick_file,
        provide_context=True,
        op_kwargs={
            "file_result": "FAILED",
            "workload_name": "",
            "message": "Ocorreu erro na execuÃ§Ã£o",
            'folder_name': 'radarScpc'
        },
        trigger_rule="one_failed",
    )

    # Criar arquivo stick de falha de pre-execuÃ§Ã£o
    stick_file_failed_pre_execution = PythonOperator(
        task_id="stick_file_failed_pre_execution",
        python_callable=utils.creat_stick_file,
        provide_context=True,
        op_kwargs={
            "file_result": "FAILED_PRE_EXECUTION",
            "workload_name": "",
            "message": "NÃ£o Ã© possÃ­vel executar a rotina novamente hoje. JÃ¡ executado e alguns arquivos foram enviados para clientes.",
            'folder_name': 'radarScpc'
        },
        trigger_rule="all_success",
    )

    # Criar Stick File de falha na criaÃ§Ã£o ou exclusÃ£o do cluster dataproc
    stick_file_failed_dataproc = PythonOperator(
        task_id="stick_file_failed_dataproc",
        python_callable=utils.creat_stick_file,
        provide_context=True,
        op_kwargs={
            "file_result": "FAILED_DATAPROC",
            "workload_name": "",
            "message": "Falha na criaÃ§Ã£o ou exclusÃ£o do cluster dataproc.",
            'folder_name': 'radarScpc'
        },
        trigger_rule="one_failed",
    )

    # Criar Stick File de falha no join dos arquivos de saida
    stick_file_failed_join = PythonOperator(
        task_id="stick_file_failed_join_files",
        python_callable=utils.creat_stick_file,
        provide_context=True,
        op_kwargs={
            "file_result": "FAILED_JOIN",
            "workload_name": "",
            "message": "Falha no join dos arquivos de saida.",
            'folder_name': 'radarScpc'
        },
        trigger_rule="one_failed",
    )

    # Criar arquivo stick de sucesso
    stick_file_success = PythonOperator(
        task_id="stick_file_success",
        python_callable=utils.creat_stick_file,
        provide_context=True,
        op_kwargs={
            "file_result": "SUCCESS",
            "workload_name": "",
            "message": "Sucesso na execuÃ§Ã£o",
            'folder_name': 'radarScpc'
        },
        trigger_rule="all_success",
    )

    # Fazer join dos arquivos parts num arquivo final como saÃ­da
    join_files_output = PythonOperator(
        task_id="join_files_output",
        python_callable=utils.join_files,
        provide_context=True,
        op_kwargs={
            "folder_name": "radarScpc"
        },
        trigger_rule="one_success",
    )

    # Verificar se Ã© possÃ­vel a execuÃ§Ã£o(se jÃ¡ foi executado e enviado arquivos naquele dia)
    check_stick_file_success = BranchPythonOperator(
        task_id="check_stick_file_success",
        python_callable=bucket_utils.check_file_task,
        provide_context=True,
        op_kwargs={
            "bucket_name": bucket_utils.BUCKET_NAME,
            "stick_status": "STICK_SUCCESS",
            "task_on_success": "stick_file_failed_pre_execution",
            "task_on_fail": "check_stick_file_fail_join",
            "folder_name": "radarScpc"
        },
        trigger_rule="all_success"
    )

    check_stick_file_fail_join = BranchPythonOperator(
        task_id="check_stick_file_fail_join",
        python_callable=bucket_utils.check_file_task,
        provide_context=True,
        op_kwargs={
            "bucket_name": bucket_utils.BUCKET_NAME,
            "stick_status": "STICK_FAILED_JOIN",
            "task_on_success": "continue_from_join",
            "task_on_fail": "create_cluster_dataproc",
            "folder_name": "radarScpc"
        },
        trigger_rule="all_success"
    )

    check_can_delete_dataproc = BranchPythonOperator(
        task_id="check_can_delete_dataproc",
        python_callable=bucket_utils.check_file_task,
        provide_context=True,
        op_kwargs={
            "bucket_name": bucket_utils.BUCKET_NAME,
            "stick_status": "STICK_FAILED_JOIN",
            "task_on_success": "finished",
            "task_on_fail": "delete_cluster_dataproc",
            "folder_name": "radarScpc"
        },
        trigger_rule="one_success"
    )

    continue_from_join = DummyOperator(task_id="continue_from_join", trigger_rule="all_success")
    finished = DummyOperator(task_id="finished", trigger_rule="all_done")

# Tasks Order

start >> get_parameters >> check_stick_file_success >> [check_stick_file_fail_join, stick_file_failed_pre_execution]

check_stick_file_fail_join >> [create_cluster_dataproc, continue_from_join]

continue_from_join >> join_files_output

create_cluster_dataproc >> [submit_job, stick_file_failed_dataproc]

submit_job >> [join_files_output, stick_file_failed]

join_files_output >> [stick_file_success, stick_file_failed_join]

stick_file_success >> check_can_delete_dataproc >> [delete_cluster_dataproc, finished]

[stick_file_failed_join, stick_file_failed] >> delete_cluster_dataproc

delete_cluster_dataproc >> [finished, stick_file_failed_dataproc]

[stick_file_failed_pre_execution, stick_file_failed_dataproc] >> finished