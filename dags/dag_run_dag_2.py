from airflow import DAG
from airflow.operators.bash import BashOperator

from datetime import datetime

dag = DAG('dag_run_dag_2', description='Teste Dag Run Other Dag',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


task1 = BashOperator(task_id='tsk1', bash_command="sleep 5", dag=dag)
task2 = BashOperator(task_id='tsk2', bash_command="sleep 5", dag=dag)



task1 >> task2