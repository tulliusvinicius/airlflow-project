from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from datetime import datetime


dag = DAG('dag_dummy', description='Exemplo Dummy',
          schedule_interval=None, 
          start_date=datetime(2023, 5, 2), 
          catchup=False)


task1 = BashOperator(task_id='tsk1', bash_command="sleep 1", dag=dag)
task2 = BashOperator(task_id='tsk2', bash_command="sleep 1", dag=dag)
task3 = BashOperator(task_id='tsk3', bash_command="sleep 1", dag=dag)
task4 = BashOperator(task_id='tsk4', bash_command="sleep 1", dag=dag)
task5 = BashOperator(task_id='tsk5', bash_command="sleep 1", dag=dag)
task_dummy = EmptyOperator(task_id='tsk_dummy', dag=dag)

[task1, task2, task3] >> task_dummy >> [task4, task5]