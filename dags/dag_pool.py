from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import datetime

dag = DAG('dag_pool', description='Exemplo de Dag Poll',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


# pool_test = variável criada lá no menu > admin > Pools

task1 = BashOperator(task_id='tsk1', bash_command="sleep 3", dag=dag, pool='pool_test')
task2 = BashOperator(task_id='tsk2', bash_command="sleep 3", dag=dag, pool='pool_test', priority_weight=5)
task3 = BashOperator(task_id='tsk3', bash_command="sleep 3", dag=dag, pool='pool_test')
task4 = BashOperator(task_id='tsk4', bash_command="sleep 3", dag=dag, pool='pool_test', priority_weight=10)