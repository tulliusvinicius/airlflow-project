from airflow import DAG, Dataset
from airflow.operators.python import PythonOperator
from datetime import datetime
import pandas as pd


dag = DAG('dag_producer', description='Exemplo de dataset "Producer"',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)



my_dataset = Dataset("/opt/airflow/data/Churn_new.csv")

def my_file():
    dataset = pd.read_csv("/opt/airflow/data/Churn.csv", sep=';')
    dataset.to_csv("/opt/airflow/data/Churn_new.csv", sep=";")
    
    
t1 = PythonOperator(task_id='t1', python_callable=my_file, dag=dag, outlets=[my_dataset])

t1