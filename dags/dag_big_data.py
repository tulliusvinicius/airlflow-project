from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator
from datetime import datetime
from big_data_operator import BigDataOperator


dag = DAG('dag_big_data', description='Exemplo de Dag Big Data com Plugin',
          schedule_interval=None, start_date=datetime(2023, 5, 2), 
          catchup=False)


big_data_task = BigDataOperator(task_id='big_data',
                           path_to_csv_file='/opt/airflow/data/Churn.csv',
                           path_to_save_file='/opt/airflow/data/Churn.json',
                           file_type='json',
                           dag=dag)

big_data_task