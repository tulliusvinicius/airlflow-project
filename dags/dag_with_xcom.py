from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from datetime import datetime


dag = DAG('dag_example_xcom', description='Exemplo com xcom',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


def task_write(**kwargs):
    kwargs['task_instance'].xcom_push(key='nome', value='neto')
    kwargs['task_instance'].xcom_push(key='idade', value=35)
    kwargs['task_instance'].xcom_push(key='CPF', value=123456789)
    kwargs['task_instance'].xcom_push(key='nacionalidade', value='brasileiro')
    
def task_read(**kwargs):
    Nome = kwargs['task_instance'].xcom_pull(key='nome')
    Idade = kwargs['task_instance'].xcom_pull(key='idade')
    cpf = kwargs['task_instance'].xcom_pull(key='CPF')
    nacionalidade = kwargs['task_instance'].xcom_pull(key='nacionalidade')
    print(f'O nome é: {Nome}')
    print(f'A idade é: {Idade}')
    print(f'O CPF é: {cpf}')
    print(f'A nacionalidade é: {nacionalidade}')

task1 = PythonOperator(task_id='tsk1', python_callable=task_write, dag=dag)

task2 = PythonOperator(task_id='tsk2', python_callable=task_read, dag=dag)



task1 >> task2
