from airflow import DAG, Dataset
from airflow.operators.python import PythonOperator
from datetime import datetime
import pandas as pd

my_dataset = Dataset("/opt/airflow/data/Churn_new.csv")

dag = DAG('dag_consumer', description='Exemplo de dataset "Consumer"',
          schedule=[my_dataset], start_date=datetime(2023, 5, 2), catchup=False)




def my_file():
    dataset = pd.read_csv("/opt/airflow/data/Churn.csv", sep=';')
    dataset.to_csv("/opt/airflow/data/Churn_new_2.csv", sep=";")
    
    
t1 = PythonOperator(task_id='t1', python_callable=my_file, dag=dag, provide_context=True)

t1