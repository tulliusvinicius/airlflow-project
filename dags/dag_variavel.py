from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.models import Variable
from datetime import datetime


dag = DAG('dag_variable', description='Exemplo de variável',
          schedule_interval=None, start_date=datetime(2023, 5, 2), catchup=False)


def print_variable(**context):
    minha_var = Variable.get("var_teste")
    print(f'O valor da variável é: {minha_var}')

task1 = PythonOperator(task_id='tsk1', python_callable=print_variable, dag=dag)




task1